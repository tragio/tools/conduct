# always pin to an explicit tag, build up a base with all of the OS
# dependencies needed by deploy and dev
FROM ubuntu:bionic-20190807 as base

# will maintain our blessed pattern regardless of whether a webapp or library
# since a library, we can pull all dev requirements into the base
# install os dependencies needed for python development and package build
RUN apt-get update && apt-get install -y --no-install-recommends \
        python3-pip \
    # reduce the size of the saved layer
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    # install suitable versions of pip and pipenv for python env management
    && pip3 install --no-cache-dir --upgrade pip==18.0 pipenv

# recommended for python in docker
ENV PYTHONUNBUFFERED 1

# direct pipenv to make venv in project dir
ENV PIPENV_VENV_IN_PROJECT 1

# prevent log pollution
ENV PIPENV_NOSPIN 1

# needed by pipenv to activate shell
ENV LANG en_US.UTF-8
ENV LC_ALL C.UTF-8
ENV SHELL /bin/bash

# provide build args of UID and USER. Matching the host's UID during dev allows
# one to yield files to host fs w/ correct perms from the container
ARG UID=1000
ARG USER=user

# build args are only at build-time...need to set inside container
ENV CONTAINER_UID=${UID}
ENV CONTAINER_USER=${USER}

# setup OS environment
RUN echo "BASE IMAGE CREATED WITH $CONTAINER_USER AS $CONTAINER_UID" \
    && useradd -lmU -u $CONTAINER_UID -s /bin/bash $CONTAINER_USER \
    && mkdir /app \
    && chown -R $CONTAINER_USER:$CONTAINER_USER /app

# set working directory to /app
WORKDIR /app

# create a layer for use by dev/build/test/doc/deploy
FROM base as dev

# set container user to unprivileged user
# NOTE: this does not happen in base so root can install additional deps
USER $CONTAINER_USER:$CONTAINER_USER

# no need to split build and deploy layers for a library
FROM dev as deploy

# copy the file system in
COPY . /app

# install deploy dependencies and make a wheel
# NOTE: depending how you split things, you may need to --dev here
# fail immediately if lock os out of date
RUN pipenv install --deploy \
    # generate the deploy requirements file
    && pipenv lock -r > requirements.txt \
    # generate the wheels for the deployment requirements
    && pipenv run pip wheel -r requirements.txt -w dist
