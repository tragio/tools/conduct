# Conduct (for Docker)

## Use Cases:

- Decouple the core services from the applications.  This is a nice pattern for
development but often is counter to real deployments.  In the spirit of
"develop like you deploy" let's enjoy the help compose offers without breaking
that rule nor writing egregious and error-prone command lines.

- Decouple build of inter-dependent services from full stack while making full
stack available for development.
